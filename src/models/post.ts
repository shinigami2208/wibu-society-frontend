export interface Post {
  caption: string
  _id?: string
  author: string
  image?: string
  created_at?: any
}
