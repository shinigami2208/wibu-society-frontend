export interface UserAuthInfo {
  username: string
  password: string
}

export interface UserInfo {
  username: string
  _id: string
}
