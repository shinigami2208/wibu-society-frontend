export interface AuthSuccessRes {
  token: string
  username: string
}
