import { createSlice } from '@reduxjs/toolkit'
import { RootState } from '../../app/store'

interface State {
  authToken: string
  username: string | null
  isAuthenticated: boolean
}

const initialState: State = {
  authToken: JSON.parse(localStorage.getItem('authToken') || '""'),
  username: null,
  isAuthenticated:
    JSON.parse(localStorage.getItem('authToken') || '""') !== '' ? true : false,
}

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    requestRegister: (state, action) => {},
    requestLogin: (state, action) => {},
    authSuccess: (state, action) => {
      let { token, username } = action.payload
      localStorage.setItem('authToken', JSON.stringify(token))

      state.username = username
      state.authToken = token
      state.isAuthenticated = true
    },
  },
})

export const authActions = authSlice.actions
export const selectAuthToken = (state: RootState) => state.auth.authToken
export const selectUsername = (state: RootState) => state.auth.username
export const selectIsAuthenticated = (state: RootState) =>
  state.auth.isAuthenticated

export default authSlice.reducer
