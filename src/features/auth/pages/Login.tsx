import { ErrorMessage, Field, Form, Formik, FormikProps } from 'formik'
import React from 'react'
import { Link } from 'react-router-dom'
import { useAppDispatch } from '../../../app/hooks'
import { authActions } from '../auth.slice'
import * as Yup from 'yup'

interface FormState {
  username: string
  password: string
}

const Login = () => {
  const dispath = useAppDispatch()

  const handleLogin = (values: FormState) => {
    const user = {
      username: values.username,
      password: values.password,
    }
    dispath(authActions.requestLogin(user))
  }

  const initialValues: FormState = {
    username: '',
    password: '',
  }

  const registerSchema = Yup.object().shape({
    username: Yup.string()
      .min(8, 'Too short')
      .max(20, 'Too long')
      .required('Required'),
    password: Yup.string()
      .min(8, 'Too short')
      .max(20, 'Too long')
      .required('Required'),
  })

  return (
    <div className="bg-slate-100 min-h-screen flex flex-col">
      <div className="container max-w-sm mx-auto flex-1 flex flex-col items-center justify-center px-2">
        <Formik
          initialValues={initialValues}
          onSubmit={handleLogin}
          validationSchema={registerSchema}
        >
          {(props: FormikProps<FormState>) => (
            <Form className="bg-white px-6 py-8 rounded shadow-md text-black w-full">
              <h1 className="mb-8 text-3xl text-center">Login</h1>
              <Field
                type="text"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="username"
                placeholder="Username"
              />
              <ErrorMessage name="username" component="div" />
              <Field
                type="password"
                className="block border border-grey-light w-full p-3 rounded mb-4"
                name="password"
                placeholder="Password"
              />
              <ErrorMessage name="password" component="div" />
              <button
                type="submit"
                className="w-full text-center py-3 rounded bg-green hover:bg-green-dark focus:outline-none my-1"
                disabled={!props.isValid && props.dirty}
              >
                Create Account
              </button>
            </Form>
          )}
        </Formik>

        <div className="text-grey-dark mt-6">
          Already have not an account?
          <Link
            className="no-underline border-b border-blue text-blue"
            to="/login"
          >
            Register
          </Link>
          .
        </div>
      </div>
    </div>
  )
}

export default Login
