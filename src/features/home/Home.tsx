import React, { useContext, useEffect } from 'react'
import Header from '../../app/components/home/layout/Header'
import LeftSideBar from '../../app/components/home/layout/LeftSideBar'
import MainContent from '../../app/components/home/layout/MainContent'
import RightSideBar from '../../app/components/home/layout/RightSideBar'
import { useAppDispatch } from '../../app/hooks'
import { SocketContext } from '../../context/socket'
import { homeActions } from './home.slice'

const Home = () => {
  const dispatch = useAppDispatch()
  const socket = useContext(SocketContext)

  useEffect(() => {
    dispatch(homeActions.getListPost())
  }, [dispatch])

  useEffect(() => {
    socket.on('CREATE_POST', () => {
      dispatch(homeActions.getListPost())
    })
  }, [socket, dispatch])

  return (
    <div>
      <Header />
      <main className="grid grid-cols-[1fr_2fr_1fr] px-5 py-10 gap-5">
        <LeftSideBar />
        <MainContent />
        <RightSideBar />
      </main>
    </div>
  )
}

export default Home
