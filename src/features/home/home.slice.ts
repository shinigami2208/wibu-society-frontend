import { createSlice } from '@reduxjs/toolkit'
import { RootState } from '../../app/store'
import { Post } from '../../models'

interface State {
  listPost: Post[]
}

const initialState: State = {
  listPost: [],
}

const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
    createPostRequest: (state, action) => {},
    getListPost: () => {},
    setListPostCreated:(state, action) => {
      state.listPost.unshift(action.payload)
    },
    setListPost: (state, action) => {
      state.listPost = action.payload
    },
  },
})

export const homeActions = homeSlice.actions
export const listPostSelector = (state: RootState) => state.home.listPost

export default homeSlice.reducer
