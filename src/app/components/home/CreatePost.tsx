import { Field, Form, Formik, FormikProps } from 'formik'
import React, { useState } from 'react'
import { homeActions } from '../../../features/home/home.slice'
import { useAppDispatch } from '../../hooks'
import { BsFillImageFill } from 'react-icons/bs'

interface PostState {
  caption: ''
  image: any
}

const CreatePost = () => {
  const [previewSource, setPreviewSource] = useState<any>()
  const dispatch = useAppDispatch()
  const initialValues: PostState = {
    caption: '',
    image: '',
  }
  const handleChangeImage = (e: any) => {
    let file = e.target.files[0]
    previewFile(file)
  }
  const previewFile = (file: any) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      setPreviewSource(reader.result)
    }
  }

  return (
    <div className="shadow-xl p-10 mb-10">
      <p className="mb-5">Bạn đang nghĩ gì?</p>
      <Formik
        initialValues={initialValues}
        onSubmit={(values, { resetForm }) => {
          if (previewSource) {
            values.image = previewSource
          }
          dispatch(homeActions.createPostRequest(values))
          resetForm({
            values: {
              caption: '',
              image: '',
            },
          })
        }}
      >
        {(props: FormikProps<PostState>) => (
          <Form>
            <div className="form-group mx-auto mb-5">
              <Field as="textarea" className="textarea w-full" name="caption" />
            </div>
            <div className="form-group mb-5">
              <div className="card border">
                <div className="flex justify-between items-center flex-row p-3">
                  <p>Thêm vào bài viết</p>
                  <div>
                    <label htmlFor="image" className="cursor-pointer">
                      <BsFillImageFill className="text-2xl" />
                    </label>
                    <input
                      type="file"
                      className="hidden"
                      id="image"
                      name="image"
                      onChange={(e: any) => {
                        props.handleChange(e)
                        handleChangeImage(e)
                      }}
                    />
                  </div>
                </div>
                {previewSource && (
                  <div className="p-5">
                    <img
                      src={previewSource}
                      alt="Preview Source"
                      className="w-100 block mx-auto object-cover"
                    />
                  </div>
                )}
              </div>
            </div>
            <div className="form-group">
              <button
                className="btn btn-block btn-outline btn-secondary"
                type="submit"
              >
                Đăng tải
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  )
}

export default CreatePost
