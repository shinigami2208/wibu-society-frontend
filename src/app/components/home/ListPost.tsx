import React from 'react'
import { listPostSelector } from '../../../features/home/home.slice'
import { Post } from '../../../models'
import { useAppSelector } from '../../hooks'
import ListPostItem from './ListPostItem'

const ListPost = () => {
  const listPost = useAppSelector(listPostSelector)

  return (
    <div>
      {listPost.map((post: Post) => (
        <ListPostItem key={post._id} post={post} />
      ))}
    </div>
  )
}

export default ListPost
