import React, { useEffect, useState } from 'react'
import userAPI from '../../../apis/userAPI'
import { Post } from '../../../models'

interface Props {
  post: Post
}

const ListPostItem = ({ post }: Props) => {
  const [detailUser, setDetailUser] = useState<any>()
  useEffect(() => {
    userAPI
      .getDetailUser(post.author)
      .then((detailUser) => {
        setDetailUser(detailUser)
      })
      .catch((error) => {
        console.log(error)
      })
  }, [post.author])

  return (
    <div className="card shadow-lg p-5">
      <div className="card-header flex items-center">
        <div className="avatar mr-3">
          <div className="w-14 mask mask-squircle">
            <img
              src="https://luv.vn/wp-content/uploads/2021/08/hinh-anh-gai-xinh-16.jpg"
              alt=""
            />
          </div>
        </div>
        <div>
          <a href="google.com">{detailUser?.username}</a>
          <p>{post?.created_at}</p>
        </div>
      </div>
      <div className="card-body">
        <p>{post?.caption}</p>
        {post.image && (
          <img
            src={post.image}
            className="w-2/3 max-w-md mx-auto"
            alt={post?.caption}
          />
        )}
      </div>
    </div>
  )
}

export default ListPostItem
