import React from 'react'
import CreatePost from '../CreatePost'
import ListPost from '../ListPost'

const MainContent = () => {
  return (
    <div>
      <CreatePost />
      <ListPost />
    </div>
  )
}

export default MainContent