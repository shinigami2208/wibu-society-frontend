import React from 'react'
import { Redirect, Route } from 'react-router'

interface Props {
  children: React.ReactNode
  isAuthenticated: boolean
  path: string
}

const PublicRouter = ({ children, isAuthenticated, path, ...rest }: Props) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        !isAuthenticated ? (
          children
        ) : (
          <Redirect to={{ pathname: '/', state: { from: location } }} />
        )
      }
    />
  )
}

export default PublicRouter
