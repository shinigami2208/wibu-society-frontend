import React from 'react'
import { Redirect, Route } from 'react-router'

interface Props {
  children: React.ReactNode
  isAuthenticated: boolean
  path: string
}

const PrivateRouter = ({ children, isAuthenticated, ...rest }: Props) => {
  return (
    <Route
      {...rest}
      render={({ location }) =>
        isAuthenticated ? (
          children
        ) : (
          <Redirect to={{ pathname: 'login', state: { from: location } }} />
        )
      }
    />
  )
}

export default PrivateRouter
