import { fork } from "redux-saga/effects";
import authSaga from "./sagas/auth";
import homeSaga from "./sagas/home";

export default function* rootSaga() {
  yield fork(authSaga)
  yield fork(homeSaga)
}