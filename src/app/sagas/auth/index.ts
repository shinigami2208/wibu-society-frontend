import { authActions } from '../../../features/auth/auth.slice'
import { requestLogin, requestRegister } from './authSaga'
import {takeEvery} from "redux-saga/effects"

export default function* authSaga() {
  yield takeEvery(authActions.requestRegister.type, requestRegister)
  yield takeEvery(authActions.requestLogin.type, requestLogin)
}
