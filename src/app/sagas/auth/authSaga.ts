import { authActions } from './../../../features/auth/auth.slice'
import { PayloadAction } from '@reduxjs/toolkit'
import { call, put } from 'redux-saga/effects'
import authAPI from '../../../apis/authAPI'
import { UserAuthInfo } from '../../../models'
import { AuthSuccessRes } from '../../../models/auth'

export function* requestRegister(action: PayloadAction<UserAuthInfo>) {
  try {
    const res: AuthSuccessRes = yield call(authAPI.register, action.payload)
    yield put(authActions.authSuccess(res))
  } catch (error) {}
}

export function* requestLogin(action: PayloadAction<UserAuthInfo>) {
  try {
    const res: AuthSuccessRes = yield call(authAPI.login, action.payload)
    yield put(authActions.authSuccess(res))
  } catch (error) {
    
  }
}
