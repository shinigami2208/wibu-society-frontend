import { PayloadAction } from '@reduxjs/toolkit'
import { call, put } from 'redux-saga/effects'
import postAPI from '../../../apis/postAPI'
import { homeActions } from '../../../features/home/home.slice'
import { Post } from '../../../models'

export function* createPostRequest(action: PayloadAction<Post>) {
  try {
    const res: Post = yield call(postAPI.createPost, action.payload)
    yield put(homeActions.setListPostCreated(res))
  } catch (error) {
    console.log(error)
  }
}

export function* getListPost() {
  try {
    const res: Post[] = yield call(postAPI.getListPost)
    yield put(homeActions.setListPost(res))
  } catch (error) {
    console.log(error)
  }
}
