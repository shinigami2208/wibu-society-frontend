import { takeEvery } from "redux-saga/effects";
import { homeActions } from "../../../features/home/home.slice";
import { createPostRequest, getListPost } from "./homeSaga";

export default function* homeSaga(){
  yield takeEvery(homeActions.createPostRequest.type, createPostRequest)
  yield takeEvery(homeActions.getListPost.type, getListPost)
}
