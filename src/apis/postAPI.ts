import { Post } from '../models'
import axiosClient from './axiosClient'

const postAPI = {
  createPost: (postData: Post): Promise<Post> => {
    const url = '/api/post/create'
    return axiosClient.post(url, postData)
  },
  getListPost: (): Promise<Post[]> => {
    const url = '/api/post'
    return axiosClient.get(url)
  },
}

export default postAPI
