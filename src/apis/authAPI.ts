import { UserAuthInfo } from '../models'
import axiosClient from './axiosClient'

const authAPI = {
  register(user: UserAuthInfo): Promise<any> {
    const url = '/api/auth/register'
    return axiosClient.post(url, user)
  },
  login(user: UserAuthInfo): Promise<any> {
    const url = '/api/auth/login'
    return axiosClient.post(url, user)
  },
}

export default authAPI
