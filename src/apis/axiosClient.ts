import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'

const token = JSON.parse(localStorage.getItem('authToken') || '""')

const axiosClient = axios.create({
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
    'Access-Control-Allow-Origin': '*',
  },
  baseURL: 'http://localhost:8000',
})

axiosClient.interceptors.request.use(
  (config: AxiosRequestConfig) => {
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

axiosClient.interceptors.response.use(
  (response: AxiosResponse) => {
    return response.data
  },
  (error) => {
    return Promise.reject(error)
  }
)

export default axiosClient
