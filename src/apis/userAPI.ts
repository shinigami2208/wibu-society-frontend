import axiosClient from "./axiosClient"

const userAPI = {
  getDetailUser: (id: string): Promise<any> => {
    const url = `/api/users/${id}`
    return axiosClient.get(url)
  }
}

export default userAPI