import React from 'react'
import { Switch } from 'react-router'
import PrivateRouter from './app/components/shared/PrivateRouter'
import PublicRouter from './app/components/shared/PublicRouter'
import { useAppSelector } from './app/hooks'
import { selectIsAuthenticated } from './features/auth/auth.slice'
import Login from './features/auth/pages/Login'
import Register from './features/auth/pages/Register'
import Home from './features/home/Home'

const App = () => {
  const isAuthenticated = useAppSelector(selectIsAuthenticated)
  return (
    <Switch>
      <PublicRouter path="/register" isAuthenticated={isAuthenticated}>
        <Register />
      </PublicRouter>
      <PublicRouter path="/login" isAuthenticated={isAuthenticated}>
        <Login />
      </PublicRouter>
      <PrivateRouter path="/" isAuthenticated={isAuthenticated}>
        <Home />
      </PrivateRouter>
    </Switch>
  )
}

export default App
