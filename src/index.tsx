import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { store } from './app/store'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { socket, SocketContext } from './context/socket'

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <SocketContext.Provider value={socket}>
        <Provider store={store}>
          <App />
        </Provider>
      </SocketContext.Provider>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
)
